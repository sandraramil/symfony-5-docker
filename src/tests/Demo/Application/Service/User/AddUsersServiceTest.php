<?php

namespace App\Tests\Demo\Application\Service\User;

use App\Demo\Application\Service\User\AddUsersCommand;
use App\Demo\Application\Service\User\AddUsersService;
use App\Demo\Domain\Model\User\User;
use App\Demo\Domain\Model\User\UserId;
use App\Demo\Domain\Model\User\UserRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class AddUsersServiceTest extends TestCase
{
    use ProphecyTrait;

    private \Prophecy\Prophecy\ObjectProphecy $userRepository;
    /**
     * @var \App\Demo\Application\Service\User\AddUsersService
     */
    private AddUsersService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->prophesize(UserRepository::class);
        $this->service = new AddUsersService($this->userRepository->reveal());
    }

    /** @test */
    public function should_call_user_repository_save() {
        $id = 12231;
        $command = new AddUsersCommand($id);
        $user = User::fromUserId(UserId::fromId($id));

        $this->service->execute($command);

        $this->userRepository->save($user)->shouldHaveBeenCalled();
    }
}
