<?php

namespace App\Tests\Demo\Infrastructure\Domain\Model\User;

use App\Demo\Domain\Model\User\User;
use App\Demo\Domain\Model\User\UserId;
use App\Demo\Domain\Model\User\UserNotFoundException;
use App\Demo\Domain\Model\User\UserRepository;
use App\Tests\IntegrationTestCase;

/**
 * @group integration
 */
class UserRepositoryPDOTest extends IntegrationTestCase
{
    /**
     * @var UserRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getService(UserRepository::class);
    }
    /** @test */
    public function should_persist_user() {
        $id = 1;
        $userId = UserId::fromId($id);
        $user = User::fromUserId($userId);

        $this->repository->save($user);

        $userFromDb = $this->repository->findByUserId($userId);
        $this->assertEquals($user->userId()->id(), $userFromDb->userId()->id());
    }

    /** @test */
    public function should_throw_exception_if_user_not_found() {
        $id = 1;
        $userId = UserId::fromId($id);
        $this->repository->remove($userId);

        $this->expectException(UserNotFoundException::class);

        $userFromDb = $this->repository->findByUserId($userId);
    }
}
