<?php

namespace App\Tests\Demo\Domain\Model\User;

use App\Demo\Domain\Model\User\UserId;
use PHPUnit\Framework\TestCase;

class UserIdTest extends TestCase
{
    /** @test */
    public function should_return_id() {
        $id = 1;
        $userId = UserId::fromId($id);

        $this->assertEquals($id, $userId->id());
    }
}
