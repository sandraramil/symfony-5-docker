<?php

namespace App\Tests\Demo\Domain\Model\User;

use App\Demo\Domain\Model\User\User;
use App\Demo\Domain\Model\User\UserId;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function should_return_user_from_user_id() {
        $id = 1;
        $userId = UserId::fromId($id);
        $user = User::fromUserId($userId);

        $this->assertEquals($userId, $user->userId());
    }
}
