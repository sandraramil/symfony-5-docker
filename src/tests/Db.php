<?php
declare(strict_types=1);

namespace App\Tests;

use Doctrine\DBAL\Connection;

class Db
{
    protected Connection $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function beginTransaction(): void
    {
        $this->conn->beginTransaction();
    }

    public function rollback(): void
    {
        $this->conn->rollBack();
    }

    public function findOne(string $table, array $identifier): ?array
    {
        $query = $this->conn->createQueryBuilder()
            ->select('*')
            ->from($table)
            ->where(key($identifier) . ' = :identifier')
            ->setParameters([
                ':identifier' => current($identifier),
            ]);

        $results = $query->execute()->fetchAll();

        return empty($results) ? null : array_shift($results);
    }

    public function insert(string $table, array $data): void
    {
        $this->conn->insert($table, $data);
    }

}
