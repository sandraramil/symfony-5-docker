<?php
declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IntegrationTestCase extends KernelTestCase
{
    protected $db;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel(['environment' => 'test']);

        $this->db = self::$container->get(\PDO::class);

        $this->db->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->db->rollBack();

        parent::tearDown();
    }

    protected function getService($id)
    {
        return self::$container->get($id);
    }

}
