<?php
declare(strict_types=1);

namespace App\Demo\Controller\API;

use App\Demo\Application\Service\User\AddUsersCommand;
use App\Demo\Application\Service\User\AddUsersService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class UsersController
{
    /**
     * @var \App\Demo\Application\Service\User\AddUsersService
     */
    private AddUsersService $addUsersService;

    public function __construct(AddUsersService $addUsersService)
    {
        $this->addUsersService = $addUsersService;
    }

    public function __invoke(Request $request)
    {
        $jsonRequest = new ParameterBag(json_decode($request->getContent(), true));
        $id = (int) $jsonRequest->get('userId');
        $this->addUsersService->execute(new AddUsersCommand($id));

        return JsonResponse::create(['Success' => "User $id has been created"]);
    }
}
