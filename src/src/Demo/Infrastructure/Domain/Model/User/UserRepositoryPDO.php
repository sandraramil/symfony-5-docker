<?php
declare(strict_types=1);

namespace App\Demo\Infrastructure\Domain\Model\User;

use App\Demo\Domain\Model\User\User;
use App\Demo\Domain\Model\User\UserId;
use App\Demo\Domain\Model\User\UserNotFoundException;
use App\Demo\Domain\Model\User\UserRepository;

class UserRepositoryPDO implements UserRepository
{
    private \PDO $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function findByUserId(UserId $userId): User
    {
        $statement = $this->pdo->prepare('SELECT * from users WHERE id = :id');
        $statement->execute(['id' => $userId->id()]);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (count($rows) <= 0) {
            throw UserNotFoundException::fromUserId($userId);
        }

        return User::fromUserId(UserId::fromId((int)$rows[0]['id']));
    }

    public function save(User $user): void
    {
        $this->pdo->prepare('INSERT INTO users(
            id)
            VALUES (
             :id
            )'
        )->execute($this->modelToArray($user));
    }

    private function modelToArray(User $user): array
    {
        return [
            'id' => $user->userId()->id(),
            ];
    }

    public function remove(UserId $userId): void
    {
        $this->pdo->prepare('DELETE FROM users WHERE id=:id'
        )->execute(['id' => $userId->id()]);
    }
}
