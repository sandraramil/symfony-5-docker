<?php
declare(strict_types=1);

namespace App\Demo\Domain\Model\User;

class User
{
    /**
     * @var \App\Demo\Domain\Model\User\UserId
     */
    private $userId;

    private function __construct(UserId $userId)
    {
        $this->userId = $userId;
    }

    public static function fromUserId(UserId $userId) : User
    {
        return new self($userId);
    }

    public function userId(): UserId
    {
        return $this->userId;
    }
}
