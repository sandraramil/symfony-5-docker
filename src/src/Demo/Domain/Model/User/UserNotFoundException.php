<?php
declare(strict_types=1);

namespace App\Demo\Domain\Model\User;

class UserNotFoundException extends \Exception
{

    public static function fromUserId(UserId $userId): self
    {
        $id = $userId->id();
        return new self("User with id $id was not found");
    }
}
