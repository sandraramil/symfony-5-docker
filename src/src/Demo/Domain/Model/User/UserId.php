<?php
declare(strict_types=1);

namespace App\Demo\Domain\Model\User;

class UserId
{
    /**
     * @var int
     */
    private $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromId(int $id)
    {
        return new self($id);
    }

    public function id(): int
    {
        return $this->id;
    }
}
