<?php
declare(strict_types=1);

namespace App\Demo\Domain\Model\User;

interface UserRepository
{
    public function findByUserId(UserId $userId) : User;
    public function save(User $user) : void;
    public function remove(UserId $userId) : void;
}
