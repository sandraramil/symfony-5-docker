<?php
declare(strict_types=1);

namespace App\Demo\Application\Service\User;

class AddUsersCommand
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}
