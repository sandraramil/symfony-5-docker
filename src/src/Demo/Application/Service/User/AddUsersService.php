<?php
declare(strict_types=1);

namespace App\Demo\Application\Service\User;

use App\Demo\Domain\Model\User\User;
use App\Demo\Domain\Model\User\UserId;
use App\Demo\Domain\Model\User\UserRepository;

class AddUsersService
{
    /**
     * @var \App\Demo\Domain\Model\User\UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(AddUsersCommand $command)
    {
        $user = User::fromUserId(UserId::fromId($command->id()));
        $this->userRepository->save($user);
    }
}
